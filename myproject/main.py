#! /usr/bin/env python2.7
# -*- coding: utf-8 -*-

import webapp2, jinja2
from os.path import dirname
from handlers import *
from lib.User import User, get_current_user
from lib import data

JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(dirname(__file__) + "/html")
)

app = webapp2.WSGIApplication([
    ('/', index.indexHandler),
    ('/login', login.loginHandler),
    ('/profile', profile.profileHandler),
    ('/interests', interests.interestsHandler),
    ('/saveinterests', saveinterests.saveinterestsHandler),
    ('/recommendation', recommendation.recommendationHandler),
    ('/loginpage', loginpage.loginpageHandler),
    ('/logout', logout.logoutHandler),
    ('/register', register.registerHandler),
    ('/adduser', adduser.adduserHandler)
], debug=True)
