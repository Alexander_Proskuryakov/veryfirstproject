from google.appengine.ext import ndb

class User(ndb.Model):
    login = ndb.StringProperty()
    password = ndb.StringProperty()
    visited = ndb.IntegerProperty(repeated=True)


def get_current_user(request):
    cookies = request.str_cookies
    if cookies.get("userid", None):
        return User.get_by_id(int(cookies["userid"]))
    else:
        return None
