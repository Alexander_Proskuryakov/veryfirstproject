import webapp2
import main

class saveinterestsHandler(webapp2.RequestHandler):
    def post(self):
        user = main.get_current_user(self.request)
        if user == None:
            self.redirect("/login")
        else:
            interests = self.request.get_all("checkes")
            user.visited = []
            for i in interests:
                t = int(i)
                if t not in user.visited:
                    user.visited.append(t)
            user.put()
            self.redirect("/profile")
            # save interests

