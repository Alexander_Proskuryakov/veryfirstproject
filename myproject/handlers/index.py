import webapp2
import main

class indexHandler(webapp2.RequestHandler):
    def get(self):
        user = main.get_current_user(self.request)
        template = main.JINJA_ENVIRONMENT.get_template("/index.html")
        context = {}
        if user == None:
            context["logined"] = False
        else:
            context["logined"] = True
        self.response.out.write(template.render(context))
