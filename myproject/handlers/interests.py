import webapp2
import main

class interestsHandler(webapp2.RequestHandler):
    def get(self):
        user = main.get_current_user(self.request)
        template = main.JINJA_ENVIRONMENT.get_template("/interests.html")
        context = {}
        if user == None:
            context["logined"] = False
            self.redirect("/")
        else:
            context["logined"] = True
            countries = list(main.data.countries.items())
            countries.sort()
            visited = set(user.visited)
            context["countries"] = [(c[1], 1 if c[0] in visited else 0) for c in countries]
            self.response.out.write(template.render(context))
