import webapp2
import main

class loginHandler(webapp2.RequestHandler):
    def post(self):
        login = self.request.get("login")
        password = self.request.get("password")
        users = main.User.query(main.User.login == login).get()
        if users == None:
            self.redirect("/login")
        else:
            userid = users.key.id()
            self.response.headers.add_header('Set-Cookie', "userid={userid}".format(userid=userid))
            self.redirect("/")
