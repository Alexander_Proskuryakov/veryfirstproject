#! /usr/bin/env python2.7
# -*- coding: utf-8 -*-

import webapp2
import main
import math

class recommendationHandler(webapp2.RequestHandler):
    def get(self):
        user = main.get_current_user(self.request)
        template = main.JINJA_ENVIRONMENT.get_template("/recommendation.html")
        if user == None:
            self.redirect("/login")
        else:
            context = {}
            context["logined"] = True
            users = main.User.query().fetch()
            out = {i:0 for i in main.data.countries}
            user_norm = math.sqrt(len(user.visited))
            if users != [] and user_norm != 0:
                user_countries = set(user.visited)
                for u in users:
                    if u.login != user.login:
                        u_norm = math.sqrt(len(u.visited))
                        common_norm = len([1 for c in u.visited if c in user_countries])
                        cos = common_norm/(u_norm*user_norm)
                        for c in u.visited:
                            out[c] += common_norm
                recommendations = list(out.items())
                recommendations.sort(key=lambda a: a[1], reverse=True)
                print(recommendations)
                print(user_countries)
                context["rec"] = [main.data.countries[r[0]] for r in recommendations if r[0] not in user_countries and r[1] > 0]
                print(context["rec"])
            else:
                context["rec"] = [u"Кажется, вы ничего не оценили или база данных сейчас пуста",]
            self.response.out.write(template.render(context))
