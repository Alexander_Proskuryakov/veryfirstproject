import webapp2
import main
import datetime

class logoutHandler(webapp2.RequestHandler):
    def get(self):
        user = main.get_current_user(self.request)
        context = {}
        if user != None:
            now = datetime.datetime.now()
            self.response.headers.add_header('Set-Cookie', 'userid=; expires={expires};'.format(expires=now))
        self.redirect("/")
