import webapp2
import main

class adduserHandler(webapp2.RequestHandler):
    def post(self):
        login = self.request.get("login")
        password = self.request.get("password")
        users = main.User.query(main.User.login == login).get()
        if users == None:
            user = main.User(login=login, password=password, visited=[])
            user.put()
            userid = user.key.id()
            self.response.headers.add_header('Set-Cookie', "userid={userid}".format(userid=userid))
            self.redirect("/")
        else:
            self.redirect("/register")
