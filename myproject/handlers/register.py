import webapp2
import main

class registerHandler(webapp2.RequestHandler):
    def get(self):
        user = main.get_current_user(self.request)
        template = main.JINJA_ENVIRONMENT.get_template("/register.html")
        context = {}
        if user == None:
            context["logined"] = False
            self.response.out.write(template.render(context))
        else:
            self.redirect("/")
