__all__ = [
    "adduser",
    "index",
    "interests",
    "login",
    "loginpage",
    "logout",
    "profile",
    "recommendation",
    "register",
    "saveinterests",
]
