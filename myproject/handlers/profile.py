import webapp2
import main

class profileHandler(webapp2.RequestHandler):
    def get(self):
        user = main.get_current_user(self.request)
        template = main.JINJA_ENVIRONMENT.get_template("/profile.html")
        context = {}
        if user == None:
            context["logined"] = False
            self.redirect("/")
        else:
            print(user)
            context["logined"] = True
            context["login"] = user.login
            context["password"] = user.password
            context["visited"] = [main.data.countries[i] for i in user.visited]
            self.response.out.write(template.render(context))
